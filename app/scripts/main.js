/*global Modernizr */
(function($, Modernizr){
	'use strict';

	$('.js-events-carousel').on('jcarousel:create jcarousel:reload', function() {
		var element = $(this),
			width = element.innerWidth();

		if (width >= 1200) {
			width = width / 4;
		} else if (width >= 600) {
			width = width / 3;
		} else if (width >= 350) {
			width = width / 2;
		}

        element.jcarousel('items').css('width', width + 'px');
    }).jcarousel({
		transitions: Modernizr.csstransitions ? {
			transforms: Modernizr.csstransforms,
			transforms3d: Modernizr.csstransforms3d,
			easing: 'ease'
		} : false
    });

	$('.jcarousel__arrow.left').jcarouselControl({
		target: '-=1'
	});
	$('.jcarousel__arrow.right').on('jcarouselcontrol:active', function(){
			$(this).removeClass('inactive');
		}).on('jcarouselcontrol:inactive', function(){
			$(this).addClass('inactive');
		}).jcarouselControl({
		target: '+=1'
	});

	$('.jcarousel__arrow.left').on('jcarouselcontrol:active', function(){
			$(this).removeClass('inactive');
		}).on('jcarouselcontrol:inactive', function(){
			$(this).addClass('inactive');
		}).jcarouselControl({
		target: '-=1'
	});

	$('.js-sidebar-btn').on('click', function(e){
			var $el = $(this),
				$parent = $el.parent();
			$el.toggleClass('is-active');
			$parent.toggleClass('is-active');
			e.preventDefault();
		});

	$('#tab-container').easytabs({
		tabs: '> .horizontal-container > ul > li',
		tabActiveClass: 'is-active'
	});
})(jQuery, Modernizr);